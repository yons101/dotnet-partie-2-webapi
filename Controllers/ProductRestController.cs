using System.Collections.Generic;
using System.Linq;
using WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Controllers
{
    [Route("/api/products")]
    public class ProductsRestController : Controller
    {
        public Repository respository { get; set; }

        public ProductsRestController(Repository repository)
        {
            this.respository = repository;
        }

        [HttpGet]
        public IEnumerable<Product> ListProducts()
        {
            return respository.Products.Include(p => p.Category);
        }

        [HttpGet("{Id}")]
        public Product GetProduct(int Id)
        {
            return respository.Products.Include(p => p.Category).FirstOrDefault(p => p.ProductID == Id);
        }

        [HttpGet("search")]
        public IEnumerable<Product> searchByKeyword(string kw)
        {
            return respository.Products.Include(p => p.Category).Where(p => p.Name.Contains(kw));
        }
        [HttpPost]
        public Product save([FromBody] Product product)
        {
            respository.Products.Add(product);
            respository.SaveChanges();
            return product;
        }
        [HttpPut("{Id}")]
        public Product update([FromBody] Product product, int Id)
        {
            product.ProductID = Id;
            respository.Products.Update(product);
            respository.SaveChanges();
            return product;
        }

        [HttpDelete("{Id}")]
        public void delete(int Id)
        {
            Product product = respository.Products.FirstOrDefault(p => p.ProductID == Id);
            respository.Products.Remove(product);
            respository.SaveChanges();
        }

        [HttpGet("paginate")]
        public IEnumerable<Product> productsPage(int page = 0, int size = 1)
        {
            int skipValue = (page - 1) * size;
            return respository.Products.Include(p => p.Category)
                                              .Skip(skipValue)
                                              .Take(size);
        }
    }
}