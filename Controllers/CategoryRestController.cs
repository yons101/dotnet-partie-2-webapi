using System.Collections.Generic;
using System.Linq;
using WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Controllers
{
    [Route("/api/categories")]
    public class CategoryRestController : Controller
    {
        public Repository respository { get; set; }

        public CategoryRestController(Repository repository)
        {
            this.respository = repository;
        }

        [HttpGet]
        public IEnumerable<Category> ListCategories()
        {
            return respository.Categories;
        }

        [HttpGet("{Id}")]
        public Category GetCategory(int Id)
        {
            return respository.Categories.FirstOrDefault(c => c.CategoryID == Id);
        }


        [HttpPost]
        public Category save([FromBody] Category category)
        {
            respository.Categories.Add(category);
            respository.SaveChanges();
            return category;
        }
        [HttpPut("{Id}")]
        public Category update([FromBody] Category category, int Id)
        {
            category.CategoryID = Id;
            respository.Categories.Update(category);
            respository.SaveChanges();
            return category;
        }

        [HttpDelete("{Id}")]
        public void delete(int Id)
        {
            Category category = respository.Categories.FirstOrDefault(c => c.CategoryID == Id);
            respository.Categories.Remove(category);
            respository.SaveChanges();
        }

        [HttpGet("{Id}/products")]
        public IEnumerable<Product> getProductsByCategory(int Id)
        {
            Category category = respository.Categories.Include(c => c.Products).FirstOrDefault(c => c.CategoryID == Id);
            return category.Products;
        }
    }
}