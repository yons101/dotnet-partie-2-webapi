using WebAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace WebAPI
{
    public class Repository: DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public Repository(DbContextOptions options):base(options) {
        }
    }
}