### Categories

![img.png](img.png)

### Products

![img_1.png](img_1.png)

### Add A Category

![img_4.png](img_4.png)

### Update A Category

![img_3.png](img_3.png)

### Delete A Category

![img_5.png](img_5.png)

### Add A Product

![img_6.png](img_6.png)

