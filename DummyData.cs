using WebAPI.Models;
using System;
namespace WebAPI.Services
{
    public static class DummyData
    {
        public static void InitData(Repository Repository)
        {
            Repository.Categories.Add(new Category { Name = "Phones" });
            Repository.Categories.Add(new Category { Name = "Computers" });

            Repository.Products.Add(new Product { Name = "iPhone 14", Price = 11000, CategoryID = 1 });

            Repository.Products.Add(new Product { Name = "Samsung J Plus", Price = 1000, CategoryID = 1 });

            Repository.Products.Add(new Product { Name = "Macbook Pro", Price = 12000, CategoryID = 2 });
            Repository.Products.Add(new Product { Name = "HP Elitebook", Price = 14000, CategoryID = 2 });
            Repository.SaveChanges();
        }
    }
}